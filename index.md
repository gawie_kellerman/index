# KelSys

## Bitbucket Details:  
* Url: https://bitbucket.org
* Login: gawie.kellerman@gmail.com
* User: gawie_kellerman
* Password: IWalkLikeYoda10$

## Projects
### skv4 - Secure Key Vault 4
#### Overview
Secure Key Vault is a JavaFX application making REST requests from a server managing
groups of keys.

#### Dependency Highlights
1. Spring Boot 2.1.0 Release Web
1. Kotlin 1.3.0
1. JDK 8
1. Gradle 4+
1. Postgres with JOOQ

#### Clone
```git clone https://gawie_kellerman@bitbucket.org/gkellermanprivate/skv4.git```


#### Notes
Try to work on it an hour every day
